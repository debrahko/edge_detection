all: process 

main_process.o: main_process.c
	gcc -c main_process.c 

original_main_process.o: original_main_process.c
	gcc -c original_main_process.c 

optimized_main_process.o: optimized_main_process.c
	gcc -c optimized_main_process.c 

png_util.o: png_util.c
	gcc -l lpng16 -c png_util.c

optimized_process: optimized_main_process.o png_util.o
	gcc -o optimized_process -lm -l png16 optimized_main_process.o png_util.o

openmp_process: main_process.o png_util.o
	gcc -o openmp_process -lm -l png16 main_process.o png_util.o

process: original_main_process.o png_util.o
	gcc -o process -lm -l png16 original_main_process.o png_util.o

test: process optimized_process openmp_process 
	./process ./images/cube.png test.png
	./optimized_process ./images/cube.png test.png
	./openmp_process ./images/cube.png test.png

clean:
	rm *.o
	rm process 
